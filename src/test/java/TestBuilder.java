import builders.ComputerBuilder;
import builders.ManualBuilder;
import computers.Computer;
import computers.Manual;
import director.Director;
import org.junit.Test;

public class TestBuilder {

    @Test
    public void testBuilderOfficePc(){
        Director director = new Director();
        ComputerBuilder computerBuilder = new ComputerBuilder();
        ManualBuilder manualBuilder = new ManualBuilder();

        director.constructOfficePc(computerBuilder);
        director.constructOfficePc(manualBuilder);

        Computer officePc = computerBuilder.getResult();
        Manual manual = manualBuilder.getResult();

        System.out.println(manual.getInfo());
    }

    @Test
    public void testBuilderGamingPc(){
        Director director = new Director();
        ComputerBuilder computerBuilder = new ComputerBuilder();
        ManualBuilder manualBuilder = new ManualBuilder();

        director.constructGamingPc(computerBuilder);
        director.constructGamingPc(manualBuilder);

        Computer gamingPc = computerBuilder.getResult();
        Manual manual = manualBuilder.getResult();

        System.out.println(manual.getInfo());
    }
}
