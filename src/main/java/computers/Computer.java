package computers;

import components.Cpu;
import components.Gpu;
import components.Motherboard;
import components.PowerSupply;

public class Computer {
    private final Type type;
    private final Cpu cpu;
    private final Gpu gpu;
    private final Motherboard motherboard;
    private final PowerSupply powerSupply;

    public Computer(Type type, Cpu cpu, Gpu gpu, Motherboard motherboard, PowerSupply powerSupply) {
        this.type = type;
        this.cpu = cpu;
        this.gpu = gpu;
        this.motherboard = motherboard;
        this.powerSupply = powerSupply;
    }

    public Type getType() {
        return type;
    }

    public Cpu getCpu() {
        return cpu;
    }

    public Gpu getGpu() {
        return gpu;
    }

    public Motherboard getMotherboard() {
        return motherboard;
    }

    public PowerSupply getPowerSupply() {
        return powerSupply;
    }
}
