package computers;

import components.Cpu;
import components.Gpu;
import components.Motherboard;
import components.PowerSupply;

public class Manual {
    private final Type type;
    private final Cpu cpu;
    private final Gpu gpu;
    private final Motherboard motherboard;
    private final PowerSupply powerSupply;

    public Manual(Type type, Cpu cpu, Gpu gpu, Motherboard motherboard, PowerSupply powerSupply) {
        this.type = type;
        this.cpu = cpu;
        this.gpu = gpu;
        this.motherboard = motherboard;
        this.powerSupply = powerSupply;
    }

    public String getInfo(){
        String template = "Brand - %s, Model - %s";
        return "Type: " + type.getType() + "\n" +
                "CPU" + String.format(template, cpu.getBrand(), cpu.getModel()) + "\n" +
                "GPU" + String.format(template, gpu.getBrand(), gpu.getModel()) + "\n" +
                "Motherboard" + String.format(template, motherboard.getBrand(), motherboard.getModel()) + "\n" +
                "Power Supply" + String.format(template, powerSupply.getBrand(), powerSupply.getModel()) + "\n";
    }
}
