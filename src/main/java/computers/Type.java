package computers;

public enum Type {
    DESKTOP("Desktop"), NOTEBOOK("Notebook"), TABLET_PC("Tablet PC");

    private String type;

    Type(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
