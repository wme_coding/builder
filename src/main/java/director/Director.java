package director;

import builders.Builder;
import components.Cpu;
import components.Gpu;
import components.Motherboard;
import components.PowerSupply;
import computers.Type;

public class Director {

    public void constructOfficePc(Builder builder){
        builder.setType(Type.DESKTOP);
        builder.setCpu(new Cpu("AMD", "Sempron 2650"));
        builder.setGpu(new Gpu("NVidia", "GeForce GT 1030"));
        builder.setMotherboard(new Motherboard("Gigabyte", "GA-A320M-H"));
        builder.setPowerSupply(new PowerSupply("Flex", "NR-PSU300F"));
    }

    public void constructGamingPc(Builder builder){
        builder.setType(Type.DESKTOP);
        builder.setCpu(new Cpu("Intel", "Core i7-9700K"));
        builder.setGpu(new Gpu("NVidia", "GeForce RTX 2080"));
        builder.setMotherboard(new Motherboard("Asus", "ROG Strix Z390-F Gaming"));
        builder.setPowerSupply(new PowerSupply("Corsair", "TX-650M"));
    }
}
