package builders;

import components.Cpu;
import components.Gpu;
import components.Motherboard;
import components.PowerSupply;
import computers.Computer;
import computers.Type;

public class ComputerBuilder implements Builder{
    private Type type;
    private Cpu cpu;
    private Gpu gpu;
    private Motherboard motherboard;
    private PowerSupply powerSupply;


    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public void setCpu(Cpu cpu) {
        this.cpu = cpu;
    }

    @Override
    public void setGpu(Gpu gpu) {
        this.gpu = gpu;
    }

    @Override
    public void setPowerSupply(PowerSupply powerSupply) {
        this.powerSupply = powerSupply;
    }

    @Override
    public void setMotherboard(Motherboard motherboard) {
         this.motherboard = motherboard;
    }

    public Computer getResult(){
        return new Computer(type, cpu, gpu, motherboard, powerSupply);
    }
}
