package builders;

import components.Cpu;
import components.Gpu;
import components.Motherboard;
import components.PowerSupply;
import computers.Type;

public interface Builder {

    void setType(Type type);
    void setCpu(Cpu cpu);
    void setGpu(Gpu gpu);
    void setPowerSupply(PowerSupply powerSupply);
    void setMotherboard(Motherboard motherboard);
}
